import random

number = random.randint(1, 100)
attempts = 0

print("Bienvenue dans le jeu de devinettes !")

while True:
    try:
        guess = int(input("Devinez le nombre (entre 1 et 100) : "))
        attempts += 1
        
        if guess > number:
            print("Trop grand !")
        elif guess < number:
            print("Trop petit !")
        else:
            print(f"Félicitations ! Vous avez deviné le nombre en {attempts} tentatives.")
            break
    
    except ValueError:
        print("Veuillez entrer un nombre valide.")

