class Budget:
    def __init__(self, montant_initial=0):
        self.solde = montant_initial
        self.depenses = []

    # Ajoute un revenu au solde du budget
    def ajouter_revenu(self, montant):
        self.solde += montant

    #Ajoute une dépense à la liste des dépenses
    def ajouter_depense(self, montant, categorie):
        if montant > self.solde:
            print("Solde insuffisant. La dépense ne peut pas être ajoutée.")
        else:
            self.solde -= montant
            self.depenses.append((montant, categorie))
    # Fonction qui retourne le solde actuel du budget
    def obtenir_solde(self):
        return self.solde

    # Fonction qui retourne la liste des dépenses
    def obtenir_depenses(self):
        return self.depenses
    
    # Fonction qui calcule et retourne le total des dépenses
    def obtenir_total_depenses(self):
        return sum(depense[0] for depense in self.depenses)

    # Fonction qui retourne les dépenses pour une catégorie donnée
    def obtenir_depenses_par_categorie(self, categorie):
        return [depense[0] for depense in self.depenses if depense[1] == categorie]
    
    # Fonction qui retourne le revenu total du budget (somme des revenus ajoutés)
    def obtenir_total_depenses_par_categorie(self, categorie):
        depenses_par_categorie = self.obtenir_depenses_par_categorie(categorie)
        return sum(depense for depense in depenses_par_categorie)

    # Fonction qui calcule le revenu total du budget 
    def obtenir_revenu(self):
        return self.obtenir_total_depenses() - self.solde
