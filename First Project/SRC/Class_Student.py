class Student: 

    compteur_student = 0  # Compteur des etudiants 
    # Constructeur
    def __init__(self,id , nom, prenon, dateN, age, niveau ):
        self.id = id
        self.nom = nom
        self.prenom = prenon
        self.dateN = dateN
        self.age = age
        self.niveau = niveau
        self.matiere_notes = {}  # Dictionnaire des notes par matière pour l'étudiant
        # Incrémenter le compteur des etudiants 
        Student.compteur_student += 1

    # Fonction qui permet d'afficher les information d'un etudiant 
    def ShowStudentInfo(self):
        print("ID :",self.id)
        print("Nom :",self.nom)
        print("Prenom :",self.prenom)
        print("Date de naissance :",self.dateN)
        print("Age :",self.age)
        print("Niveau d'étude :",self.niveau)

    # Fonction qui permet de calculer le nombre de proffesseur lié à l'école FOFO
    @classmethod
    def nombre_student(cls):
        return Student.compteur_student
    
    #Fontion qui permet d'ajouter une note d'une matiere : 
    def ajouter_note_matiere(self, matiere, note):
        self.matiere_notes[matiere] = note
    
    # Fontion qui permet d'afficher les notes des different matieres d'un étudiant 
    def afficher_notes_matiere(self, matiere):
        if matiere in self.matiere_notes:
            print("Notes de l'étudiant", self.nom, "pour la matière", matiere.nom)
            print("Note :", self.matiere_notes[matiere])
        else:
            print("Aucune note trouvée pour la matière", matiere.nom)

    # Fonction qui permet de calculet la moyenne général d'un étudiant
    def calculer_moyenne_generale(self):
        if len(self.matiere_notes) == 0:
            print("Aucune note disponible.")
            return
        
        somme_notes = sum(self.matiere_notes.values())
        nombre_matieres = len(self.matiere_notes)
        moyenne_generale = somme_notes / nombre_matieres
        
        return moyenne_generale

        



