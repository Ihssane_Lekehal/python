class Personnel:
    ecole = "Ecole FOFO"  # Variable de classe

    def __init__(self, nom, prenom, dateN, matricule, salaire, grade, email):
        self.nom = nom
        self.prenom = prenom
        self.dateN = dateN
        self.matricule = matricule
        self.salaire = salaire
        self.grade = grade
        self.email = email

    def afficher_informations(self):
        print("Nom de l'école :", Personnel.ecole)
        print("Nom :", self.nom)
        print("Prénom :", self.prenom)
        print("Date de naissance :", self.dateN)
        print("Matricule :", self.matricule)
        print("Salaire :", self.salaire)
        print("Grade :", self.grade)
        print("Email :", self.email)