class Matiere:
    def __init__(self, nom, code):
        self.nom = nom
        self.code = code
        self.notes = {}  # Dictionnaire des notes des étudiants pour cette matière
    
    def ajouter_note_etudiant(self, etudiant, note):
        self.notes[etudiant] = note
    
    def afficher_notes(self):
        print("Notes pour la matière", self.nom)
        for etudiant, note in self.notes.items():
            print("Étudiant ", etudiant.nom, " ", etudiant.prenom," :")
            print("Note :", note)
            print()
