from Class_Student import Student
from Class_Personnel import Personnel
from Class_Professeur import Professeur
from Class_PersonnelAdministration import PersonnelAdministration
from Class_Budget import Budget
from Class_Matiere import Matiere

if __name__ == "__main__":

    # Test création des objets type Student ###########################################################
    print("********************************************")
    Student1 = Student(1,"YOYO", "sara","10/07/2013","10", "1er")
    Student2 = Student(2,"HAMOU", "Rachid","15/08/2012","11", "2eme")
    Student3 = Student(3,"SAHO", "koyaki","29/08/2011","12", "3eme")
    print("\n Student 1 : \n")
    Student1.ShowStudentInfo()
    print("\n Student 2 : \n")
    Student2.ShowStudentInfo()
    print("\n Student 3 : \n")
    Student3.ShowStudentInfo()
    print()  # Saut de ligne
    nombre_student = Student.nombre_student()
    # Afficher le nombre des etudiants
    print("Nombre de professeurs :", nombre_student)
    print()


    # Test Création des objets type Professeur ######################################################
    print("********************************************")
    prof1 = Professeur("Dupont", "Jean", "05/10/1978", "12345", 2500, "Maître de conférences","dupont.jean@ecole-fofo.fr","Anglais")
    prof2 = Professeur("Martin", "Sophie", "12/07/1985", "67890", 2800, "Professeur agrégé","martin.sophie@ecole-fofo.fr","Informatique")
    print("\n Prof 1 : \n")
    prof1.ShowProfInfo()
    print("\n Prof 2 : \n")
    prof2.ShowProfInfo()
    print()  # Saut de ligne
    nombre_professeurs = Professeur.nombre_professeurs()
    # Afficher le nombre de prof
    print("Nombre de professeurs :", nombre_professeurs)
    print()

    # Test Création des objets type personnel Administration  ######################################################
    print("********************************************")
    per1 = PersonnelAdministration("Aurelie", "Labor", "05/10/1978", "12345", 2700,"Responsable financier" ,"aurelie.labor@ecole-fofo.fr",35)
    per2 = PersonnelAdministration("Samou", "Timermo", "12/07/1985", "67890", 5000, "Directeur","samou.Timermo@ecole-fofo.fr",40)
    print("\n Perso Admin 1 : \n")
    per1.ShowProfInfo()
    print("\n Perso Admin 2 : \n")
    per2.ShowProfInfo()
    print()  # Saut de ligne
    nombre_persoAdmin = PersonnelAdministration.nombre_personnelAdmin()
    # Afficher le nombre de pers Admin
    print("Nombre de personnels d'administartion :", nombre_persoAdmin)
    print()
    print()


    # Tester d'ajouter objet type matieres :
    print("********************************************")
    maths = Matiere("Mathématiques", "MATH101")
    francais = Matiere("Français", "FRAN201")
    anglais = Matiere("Anglais", "ANGL301")
    informatique = Matiere("Informatique","INFO401")
    # Attribution des notes pour les étudiants dans les matières correspondantes
    Student1.ajouter_note_matiere(maths, 15)
    Student1.ajouter_note_matiere(francais, 13)
    Student1.ajouter_note_matiere(anglais, 8)
    Student1.ajouter_note_matiere(informatique, 18)

    maths.ajouter_note_etudiant(Student1, 15)
    francais.ajouter_note_etudiant(Student1, 13)
    anglais.ajouter_note_etudiant(Student1, 8)
    informatique.ajouter_note_etudiant(Student1, 18)

    Student2.ajouter_note_matiere(maths, 12)
    Student2.ajouter_note_matiere(francais, 11)
    Student2.ajouter_note_matiere(anglais, 19)
    Student2.ajouter_note_matiere(informatique, 10)
    
    maths.ajouter_note_etudiant(Student2, 12)
    francais.ajouter_note_etudiant(Student2, 11)
    anglais.ajouter_note_etudiant(Student2, 19)
    informatique.ajouter_note_etudiant(Student2, 10)

    Student3.ajouter_note_matiere(maths, 15)
    Student3.ajouter_note_matiere(francais, 18)
    Student3.ajouter_note_matiere(anglais, 17)
    Student3.ajouter_note_matiere(informatique, 20)

    maths.ajouter_note_etudiant(Student3, 15)
    francais.ajouter_note_etudiant(Student3, 18)
    anglais.ajouter_note_etudiant(Student3, 17)
    informatique.ajouter_note_etudiant(Student3, 20)

    # Affichage des informations des étudiants
    print("Notes Student 1 \n")
    Student1.ShowStudentInfo()
    print("Notes Student 2 \n")
    Student2.ShowStudentInfo()
    print("Notes Student 3   \n")
    Student3.ShowStudentInfo()

   # Affichage des notes par matière pour les étudiants
    print()
    print()
    print("Affichage des notes par matière pour les étudiants")
    print("Notes matière 'maths':")
    maths.afficher_notes()
    print("Notes matière 'francais':")
    francais.afficher_notes()
    print("Notes matière 'anglais':")
    anglais.afficher_notes()
    print("Notes matière 'informatique':")
    informatique.afficher_notes()
    print()

    # Affichage des notes de l'étudiant pour une matière donnée
    print("Affichage des notes de l'étudiant pour une matière donnée")
    print("Note Student 1 en 'maths':")
    Student1.afficher_notes_matiere(maths)
    print("Note Student 2 en 'francais':")
    Student2.afficher_notes_matiere(francais)
    print("Note Student 3 en 'informatique':")
    Student3.afficher_notes_matiere(informatique)
    print()

    #Aficher moyenne générale des étudiants
    moyenneStd1 = Student1.calculer_moyenne_generale()
    print("Moyenne générale de Student 1 :", moyenneStd1)

    moyenneStd2 = Student2.calculer_moyenne_generale()
    print("Moyenne générale de Student 2 :", moyenneStd2)

    moyenneStd2 = Student3.calculer_moyenne_generale()
    print("Moyenne générale de Student 2 :", moyenneStd2)

    # Tester les fonction de la classe Budget pour la gestion du budget ##############################
    print("********************************************")
    # Création de l'objet Budget avec un montant initial de 50000 eurosbudget = Budget(50000)
    budget = Budget(50000)

    # Ajout des salaires des professeurs et perso d'administration comme dépenses
    budget.ajouter_depense(prof1.salaire, "Salaire des professeurs")
    budget.ajouter_depense(prof2.salaire, "Salaire des professeurs")
    budget.ajouter_depense(per1.salaire, "Salaire des personnels d'administration")
    budget.ajouter_depense(per2.salaire, "Salaire des personnels d'administration")

    # Ajout de quelques dépenses pour différentes catégories
    budget.ajouter_depense(200, "Alimentation")
    budget.ajouter_depense(1000, "Logement")
    budget.ajouter_depense(500, "Transport")
    budget.ajouter_depense(300, "Loisirs")
    budget.ajouter_depense(400, "Santé")
    budget.ajouter_depense(800, "Autres")

    # Affichage du solde actuel du budget
    solde_actuel = budget.obtenir_solde()
    print("Solde actuel du budget :", solde_actuel)
    print()

    # Affichage des dépenses par catégorie
    categories = ["Salaire des professeurs","Salaire des personnels d'administration","Alimentation", "Logement", "Transport", "Loisirs", "Santé", "Autres"]
    for categorie in categories:
        total_depenses_categorie = budget.obtenir_total_depenses_par_categorie(categorie)
        print("Total des dépenses pour la catégorie", categorie, ":", total_depenses_categorie)
    print()

    # Affichage de la liste des dépenses
    liste_depenses = budget.obtenir_depenses()
    #print("Liste des dépenses :", liste_depenses)
    print("Liste des dépenses :")
    for depense in liste_depenses:
        montant, categorie = depense
        print("- Montant :", montant)
        print("  Catégorie :", categorie)
    print()

    # Affichage de la somme des depenses 
    total_depense = budget.obtenir_total_depenses()
    print("Total dépenses :", total_depense)
    print()

