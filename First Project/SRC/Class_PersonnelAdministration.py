from Class_Personnel import Personnel

class PersonnelAdministration(Personnel):
    compteur_perso_Admin = 0  # Compteur de personnels d'administration 
    def __init__(self, nom, prenom, dateN, matricule, salaire, grade, email, NbrHeure):
        super().__init__(nom, prenom, dateN, matricule, salaire, grade, email)
        self.NbrHeure = NbrHeure
        # Incrémenter le compteur du nombre de personnel d'administration 
        PersonnelAdministration.compteur_perso_Admin += 1

    # Fonction qui permet d'afficher info personnel d'administration 
    def ShowProfInfo(self):
        super().afficher_informations()
        print("Nombre d'heures de travail par semaine :", self.NbrHeure)
    

    # Fonction qui permet de calculer le nombre de personnels d'administration  lié à l'école FOFO
    @classmethod
    def nombre_personnelAdmin(cls):
        return PersonnelAdministration.compteur_perso_Admin
